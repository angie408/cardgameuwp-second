﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    struct GameScore
    {
        private int _playerScore;

        private int _houseScore;

    }

    class CardGame
    {
        private CardDeck _deck;

        private GameScore _score;

        private Card _playerCard;

        private Card _houseCard;
    }
}
