﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalCardGame
{
    enum CardSuit
    {
        Diamonds = 1,
        Hearts,
        Clubs,
        Spades
    }

    /// <summary>
    /// Represents a card in a card game
    /// </summary>
    class Card
    {
        /// <summary>
        /// The value of the card: 1 - 13
        /// </summary>
        private byte _value;

        /// <summary>
        /// The suit of the card
        /// </summary>
        private CardSuit _suit;

        /// <summary>
        /// Maximum card value allowed in a game for this type of card
        /// </summary>
        private const int MAX_CARD_VALUE = 13;

        /// <summary>
        /// Maximum suit count the cards support
        /// </summary>
        private const int MAX_SUIT_COUNT = 4;
    }
}
